package main

import "fmt"

// interface Logger có method Log
type Logger interface {
	Log(message string)
}

// struct ConsoleLogger implements interface Logger
type ConsoleLogger struct{}

func (l ConsoleLogger) Log(message string) {
	fmt.Println(message)
}

// struct Application sử dụng Logger
type Application struct {
	logger Logger
}

func (a *Application) SetLogger(logger Logger) {
	a.logger = logger
}

func (a *Application) Run() {
	a.logger.Log("Application is running.")
}

func main() {
	app := &Application{}
	app.SetLogger(ConsoleLogger{})
	app.Run()
}